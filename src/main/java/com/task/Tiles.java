package com.task;

import com.task.model.Squares;
import javafx.scene.image.Image;

public class Tiles {
  public static final Image BLANK = loadImage("blank.png");
  public static final Image FLAG = loadImage("flag.png");
  public static final Image EXPOSED = loadImage("cleared.png");
  public static final Image MINE = loadImage("mine.png");
  public static final Image HITMINE = loadImage("hitmine.png");
  public static final Image WRONGMINE = loadImage("wrongmine.png");
  private static final Image[] digits = new Image[9];

  static {
    digits[0] = EXPOSED;

    for(int i=1; i < digits.length; i++) {
      digits[i] = loadImage(String.format("%d.png", i));
    }
  }

  private Tiles() {
  }

  public static Image getImage(Squares square) {
    switch (square) {
    case BLANK:
      return Tiles.BLANK;
    case FLAG:
      return Tiles.FLAG;
    case MINE:
      return Tiles.MINE;
    case CLEARED:
      return Tiles.EXPOSED;
    case HITMINE:
      return Tiles.HITMINE;
    case WRONGMINE:
      return Tiles.WRONGMINE;
    default:
      throw new AssertionError("Неизвестный тип клетки: " + square);
    }
  }

  public static Image getDigit(int index) {
    //Check
    if (index < 0 || index > 8) throw new IndexOutOfBoundsException("Incorrect tile number!" + index);

    return digits[index];
  }

  private static Image loadImage(String path) {
    return new Image(Tiles.class.getResourceAsStream("/resources/" + path));
  }
}
