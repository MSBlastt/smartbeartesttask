package com.task;

import javafx.scene.Parent;

@FunctionalInterface
public interface HasParent {
  Parent asParent();
}
