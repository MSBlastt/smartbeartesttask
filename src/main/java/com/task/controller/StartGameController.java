package com.task.controller;

import com.task.SapperPane;
import com.task.model.Minefield;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class StartGameController {

    @FXML
    Parent root;

    @FXML
    Button startGameButton;

    @FXML
    TextField widthField;

    @FXML
    TextField heightField;

    @FXML
    TextField minesCountField;

    @FXML
    Label errorLabel;

    @FXML
    Label timeLabel;

    @FXML
    Pane minefieldPane;


    public void checkParameters() {

        String widthText = widthField.getText().replaceAll("[^\\d]", "");
        String heigthText = heightField.getText().replaceAll("[^\\d]", "");
        String minesText = minesCountField.getText().replaceAll("[^\\d]", "");

        if ("".equals(widthText) || "".equals(heigthText) || "".equals(minesText)) {
            errorLabel.setText("Введите числа от 10 до 50.");
            return;
        }

        int width = Integer.parseInt(widthText);
        int heigth = Integer.parseInt(heigthText);
        int mines = Integer.parseInt(minesText);

        if (width < 10 || width > 50 || heigth < 10 || heigth > 50 || mines < 1) {
            errorLabel.setText("Введите числа от 10 до 50. Кол-во мин: (ширина * высота) - 1");
            return;
        }

        if ((width * heigth) <= mines) {
            errorLabel.setText("Количество мин не может быть больше чем " + ((width * heigth) - 1));
            return;
        }

        startGame(heigth, width, mines);
    }

       public void startGame(int width,int heigth, int mines){
        startGameButton.setText("Новая игра");
        errorLabel.setText("");
        minefieldPane.setVisible(true);
        if (timer != null) timer.cancel();
        timerStarted = false;

           //Change size
        Stage stage = (Stage) root.getScene().getWindow();
        stage.setWidth((heigth * 24) + 250);
        stage.setHeight((width * 24) + 300);

           //Centering game
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenSizeWidth = screenSize.getWidth();
        double screenSizeHeight = screenSize.getHeight();
        stage.setX((screenSizeWidth - stage.getWidth()) / 2);
        stage.setY((screenSizeHeight - stage.getHeight()) / 2);

           //Start game
        prepareTimer();
        generateMinefield(width, heigth, mines);
    }


    private void generateMinefield(int rows, int columns, int mines) {

        if (minefieldPane.getChildren().size() != 0) minefieldPane.getChildren().clear();

        Minefield minefield = new Minefield(rows, columns, mines);

        SapperPane minesweeper = new SapperPane(minefield, this);

        minesweeper.asParent().requestFocus();

        this.minefieldPane.getChildren().add(minesweeper.asParent());

    }


    //Timer countdown
    private Timer timer;
    boolean timerStarted;

    private void prepareTimer() {
        timeLabel.setLayoutX(root.getScene().getWindow().getWidth() / 2);
        timeLabel.setText(String.valueOf(0));
        timeLabel.setVisible(true);

    }

    public void startTimer() {

        //Starting countdown
        timerStarted = true;
        final Integer[] countdown = {0};
        timer = new java.util.Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                Platform.runLater(() -> timeLabel.setText((Integer.toHexString(++countdown[0])).toUpperCase())); // -.-
            }
        }, 0, 1000); // 0 sec delay, 1 sec period
 }

    public void stopTimer(){
        timer.cancel();
    }

    public boolean isTimerStarted() {
        return timerStarted;
    }

}


