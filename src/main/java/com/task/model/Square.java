package com.task.model;

import java.util.List;

public class Square {
  private final int column;
  private final int row;
  private final Minefield minefield;
  private boolean mine;
  private Squares type = Squares.BLANK;
  private int nearbyMines;

  Square(Minefield minefield, int row, int column) {
    this.minefield = minefield;
    this.row = row;
    this.column = column;
  }

  public Squares getType() {
    return type;
  }

  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }

  public int getMineCount() {
    return nearbyMines;
  }

  public boolean isRevealable() {
    return !minefield.isGameOver() && type == Squares.BLANK;
  }

  public void switchFlag() {
    if (minefield.isGameOver()) {
      return;
    }

    if (type == Squares.FLAG) {
      type = Squares.BLANK;
    } else if (type == Squares.BLANK) {
      type = Squares.FLAG;
    } else {
      return;
    }

    minefield.updateSquare(this);
  }

  public void reveal() {
    if (type != Squares.BLANK || minefield.isGameOver()) {
      return;
    }

    if (mine) {
      type = Squares.HITMINE;
      mine = false;
      minefield.onGameLost();
    } else {
      minefield.reveal(this);
    }
  }

  public void revealNearby() {
    if (minefield.isGameOver() || type != Squares.CLEARED) {
      return;
    }

    List<Square> neighbors = minefield.findNeighbors(this);
    int nearbyFlags = 0;

    for (Square square : neighbors) {
      if (square.type == Squares.FLAG) {
        nearbyFlags++;
      }
    }

    if (nearbyFlags == nearbyMines) {
      for (Square square : neighbors) {
        square.reveal();
      }
    }
  }

  void addNearbyMine() {
    nearbyMines++;
  }

  boolean isMine() {
    return mine;
  }

  void setMine(boolean isMine) {
    mine = isMine;
  }

  void onGameLost() {
    if (mine) {
      type = Squares.MINE;
    } else if (type == Squares.FLAG) {
      type = Squares.WRONGMINE;
    }
  }

  void onGameWon() {
    if (mine) {
      type = Squares.FLAG;
    }
  }

  int visit() {
    int exposed = 1;
    type = Squares.CLEARED;

    if (nearbyMines == 0) {
      for (Square square : minefield.findNeighbors(this)) {
        if (square.type != Squares.CLEARED) {
          exposed += square.visit();
        }
      }
    }

    return exposed;
  }
}
