package com.task.model;

public enum Squares {
  BLANK, FLAG, MINE, HITMINE, WRONGMINE, CLEARED
}
