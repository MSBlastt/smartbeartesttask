package com.task.model;

//For handlers removing
@FunctionalInterface
public interface HandlerRegistration {

  void removeHandler();
}
