package com.task;

import com.task.controller.StartGameController;
import com.task.model.Minefield;
import com.task.model.Minefield.FieldHandler;
import com.task.model.Minefield.State;
import com.task.model.Square;
import com.task.model.Squares;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.PaneBuilder;
import javafx.scene.text.Font;

public final class SapperPane implements HasParent {
  private final Parent root;
  private final Label status;
  private final int rows;
  private final int columns;
  private final Minefield field;
  private final FieldCanvas canvas;
  private final StartGameController controller;


  public SapperPane(Minefield field, StartGameController controller) {
    this.field = field;
    rows = field.getRowCount();
    columns = field.getColumnCount();
    this.controller = controller;

    canvas = new FieldCanvas();
    canvas.setLayoutX(14);
    canvas.setLayoutY(49.0);
    canvas.setWidth(24 * columns);
    canvas.setHeight(24 * rows);

    canvas.setOnMouseClicked(this::onCanvasClicked);

    canvas.setOnMousePressed(this::onCanvasPressed);

    root = PaneBuilder.create()
        .style("-fx-border-color: black;"
            + "-fx-border-width: 1;"
            + "-fx-border-radius: 6;"
            + "-fx-padding: 6;"
            + "-fx-background-color: white;")
        .prefHeight((26 * rows) + 30)
        .prefWidth((26 * columns) + 8)
        .children(
                HBoxBuilder.create()
                        .layoutX(14)
                        .layoutY(14)
                        .spacing(5)
                        .children().build(),
                canvas,
                status = LabelBuilder.create()
                        .text("")
                        .layoutX(14)
                        .layoutY(14).build()
        ).build();
      status.setFont(Font.font(18));

    field.addFieldHandler(new FieldHandler() {
      @Override
      public void updateSquare(Square square) {
        drawSquare(square);
      }

      @Override
      public void updateBoard() {
        drawBoard();
      }

      @Override
      public void changeState(State state) {
        updateText(state);
      }
    });
  }

  public Parent asParent() {
    return root;
  }

  private void onCanvasClicked(MouseEvent event) {
    if (!controller.isTimerStarted()) controller.startTimer();
    Square square = findSquare(event);
    int clicks = event.getClickCount();
    MouseButton button = event.getButton();

    if (button == MouseButton.MIDDLE
        || (clicks == 2 && button == MouseButton.PRIMARY)) {
      square.revealNearby();
    } else if (clicks == 1 && button == MouseButton.PRIMARY) {
      canvas.clearSelection();
      square.reveal();
    }
  }

  private void onCanvasPressed(MouseEvent event) {
    Square square = findSquare(event);
    int row = square.getRow();
    int column = square.getColumn();

    if (event.isSecondaryButtonDown()) {
      square.switchFlag();
    } else if (event.isPrimaryButtonDown() && square.isRevealable()) {
      canvas.setSelection(row, column);
    }
  }

  private void drawSquare(Square square) {
    Image image = square.getType() == Squares.CLEARED ? Tiles.getDigit(square
        .getMineCount()) : Tiles.getImage(square.getType());

    canvas.drawImage(square.getRow(), square.getColumn(), image);
  }

  private void updateText(Minefield.State state) {
    String text;

    switch(state) {
    case LOSE:
      text = "К сожалению, вы проиграли.";
        controller.stopTimer();
      break;
    case WON:
      text = "Примите поздравления, вы выиграли!";
        controller.stopTimer();
      break;
    default:
      text = "";
    }

    status.setText(text);
  }

  private void drawBoard() {
    for (int row = 0; row < rows; row++) {
      for (int column = 0; column < columns; column++) {
        drawSquare(field.getSquare(row, column));
      }
    }
  }

  private Square findSquare(MouseEvent event) {
    return field.getSquare(canvas.scaleRow(event.getY()),
        canvas.scaleColumn(event.getX()));
  }


}
